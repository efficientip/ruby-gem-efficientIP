require "#{File.dirname(__FILE__)}/ruby-gem-efficientIP/lib/SOLIDserver.rb"
require 'json'
require 'logger'

apiendpoint = SOLIDserver::SOLIDserver.new('sds01.home', 'ipmadmin', 'admin', logger: Logger.new(STDOUT, level: Logger::ERROR ))

res=apiendpoint.ip_address_list(limit: 10).body
jres = JSON.parse(res)

jres.each do |child|
    printf("%s %s %s\n", child["site_name"], child['hostaddr'], child['name'])
end


# puts apiendpoint.doc()